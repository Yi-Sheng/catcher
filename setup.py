from setuptools import setup, find_packages

with open('./requirements.txt') as req_f:
    requirements = [line.rstrip() for line in req_f]


package_data = {
    'catcher': [ 'templates/*.html', 'static/*' ]
}

setup(
    name='catcher',
    version='0.0.1',

    author='Yi-Sheng Fu',
    author_email='ethanfu@macrodatalab.com',
    maintainer='Yi-Sheng Fu',
    maintainer_email='ethanfu@macrodatalab.com',

    description='SSE server',

    packages=find_packages(),
    include_package_data=True,
    package_data=package_data,

    install_requires=requirements,

    entry_points = {
        'console_scripts': [
            'catcher = catcher.catcher:main'
        ]
    },

    platforms=['Independent'],
    zip_safe=False
)
