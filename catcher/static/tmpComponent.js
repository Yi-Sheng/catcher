(function(window,undefined) {
    window.demo = {};
    window.demo.getData = function(cmdStr,callback){
        return getData(cmdStr,callback);
    };

    var conn = {target:'http://127.0.0.1:9999'}
    var bufferData = [];
    var getData = function(cmdStr,callback) {
        var cmdJson = cmd2JSON(cmdStr);
        $.post(conn.target + "/cmd",JSON.stringify(cmdJson),"json")
        .done(function(data){
                if(data[0]['Status']>=0){
                      var scankey = JSON.parse(data[0]['Content']);
                     if (scankey.length!=undefined){
                              bufferData=[];
                              var args={};
                              args.callback=callback;
                              var container ,settingInput;
                              scanData(scankey,1,container,settingInput,args);
                      }
                }
        })
        .fail(function(xhr, textStatus, errorThrown){
                console.log(textStatus);
       });
      
    };



   var cmd2JSON = function(cmdStr){
        var cmdJson = {
                   "Stmt":cmdStr,
                   "Workspace":conn.workspace,
                   "Opts":conn.opts
        }
        console.log(cmdJson);
        return cmdJson;
    }


    var appendData = function(data){
       for (var i = 0; i < data.length; i++){
             bufferData.push(data[i]);
       }
    };


    var scanData = function(scankey,index,container, settingInput,args){
                  var cmdJson =  { 'Stmt':'scan '+ [scankey,index, 1000].join(' ') , 'Workspace':"",'Opts':""  };
                  $.post(conn.target+"/cmd",JSON.stringify(cmdJson),"json")
                        .done(function(data){
                               var data = JSON.parse(data[0]['Content']);
                               if(data.index!=-1){
                                   appendData(data.content);
                                   scanData(scankey,data.index,container,settingInput);
                               }else{
                                   appendData(data.content);
                                   args.callback(bufferData.slice(),container,settingInput,args);
                                   bufferData=[];
                               }
                         });
    } 
})(window);
