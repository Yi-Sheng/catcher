FROM dev.bigobject.io/macrodata/python

COPY . /catcher
WORKDIR /catcher
RUN pip install /catcher
CMD ["catcher"]

