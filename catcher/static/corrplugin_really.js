(function(window, undefined) {

    var history = [];
    var conn = {};
    var bufferData = [];

    window.corr = {
        version: "demo_only"
    };

    window.corr.bourl = function(url) {
        return bourlFunc(url);
    };

    window.corr.getTwitterData = function(cmdStr,callback){
        return getTwitterData(cmdStr,callback);
    };




    var bourlFunc = function(bourl) {
        var first_arg = bourl;
        if (first_arg !== undefined) {
            conn.scheme = url("protocol", first_arg) == 'bos' ? 'https' : 'http';
            conn.host = url("hostname", first_arg);
            conn.port = url("port", first_arg);
            conn.token = url("pass", first_arg);
            conn.path = url("path", first_arg);
            conn.original = first_arg;

            if (conn.port == 80 || conn.port == 443) {
                conn.target = conn.scheme + '://' + conn.host + conn.path;
            } else {
                conn.target = conn.scheme + '://' + conn.host + ':' + conn.port + conn.path;
            }
        }
    };




    function add2history(cmdStr){
         var header = $('<div class="col-md-11"><h3><span class="label label-info"></span></h3></div>');
         header.children().children().html(cmdStr);
         var history = $("#cmdhistory");
         if ($("#cmdhistory > div").length > 2){
             history.find('div').first().remove(); 
         }
         history.append(header);
    };



    var getTwitterData = function(cmdStr,callback) {
        var cmdJson = cmd2JSON(cmdStr);
        $.post(conn.target + "/cmd",JSON.stringify(cmdJson),"json")
        .done(function(data){
                if(data[0]['Status']>=0){
                      var scankey = JSON.parse(data[0]['Content']);
                     if (scankey.length!=undefined){
                              bufferData.length=0;
                              var args={};
                              args.callback=callback;
                              var container ,settingInput;
                              scanData(scankey,1,container,settingInput,args);
                      }
                }
        })
        .fail(function(xhr, textStatus, errorThrown){
       });
      
    };




    var cmd2JSON = function(cmdStr){
        var cmdJson = {
                   "Stmt":cmdStr,
                   "Workspace":conn.workspace,
                   "Opts":conn.opts
        }
        return cmdJson;
    }


    var appendData = function(data){
       for (var i = 0; i < data.length; i++){
             bufferData.push(data[i]);
       }
    };


    var scanData = function(scankey,index,container, settingInput,args){
                  var cmdJson =  { 'Stmt':'scan '+ [scankey,index, 1000].join(' ') , 'Workspace':"",'Opts':""  };
                  $.post(conn.target+"/cmd",JSON.stringify(cmdJson),"json")
                        .done(function(data){
                               var data = JSON.parse(data[0]['Content']);
                               if(data.index!=-1){
                                   appendData(data.content);
                                   scanData(scankey,data.index,container,settingInput);
                               }else{
                                   appendData(data.content);
                                   args.callback($.extend(true, [], bufferData),container,settingInput,args);
                               }
                         });
    } 


    var loadEdgeData = function (args,settingInput, container,callback){
        var cmdStr = "GET FACT('" + args.keyword + "' to '" + args.targetword + "') FROM " + args.assoc;
        add2history(cmdStr);
        var cmdJson = cmd2JSON(cmdStr);
        $.post(conn.target + "/cmd",JSON.stringify(cmdJson),"json")
        .done(function(data){
                if(data[0]['Status']>=0){
                      var scankey = JSON.parse(data[0]['Content']);
                      if (scankey.length!=undefined){
                              bufferData.length=0;
                              args.callback = callback;
                              scanData(scankey,1,container,settingInput,args);
                      }
                }
        })
        .fail(function(xhr, textStatus, errorThrown){
       });
    };


    var loadData = function (args,settingInput, container,callback){
        var cmdStr = "get top 15 freq('" + args.keyword + "') from " + args.assoc;
        add2history(cmdStr);
        var cmdJson = cmd2JSON(cmdStr);
        $.post(conn.target + "/cmd",JSON.stringify(cmdJson),"json")
        .done(function(data){
                if(data[0]['Status']>=0){
                      var scankey = JSON.parse(data[0]['Content']);
                     if (scankey.length!=undefined){
                              bufferData.length=0;
                              args.callback=callback;
                              scanData(scankey,1,container,settingInput,args);
                      }
                }
        })
        .fail(function(xhr, textStatus, errorThrown){
       });
    };

   
    window.$.fn.correlationTree = function(args, settingInput){
               var container = d3.select(this[0]);
               loadData(args,settingInput,container,drawCorr);
    }

    var drawCorr = function(derivedData,container, settingInput,args) {
        draw(derivedData);

        function draw(derivedData) {
            // Data settings
            var setting = initCorrSetting(settingInput, derivedData),
                data = _.sortBy(derivedData, function(num) {
                    return -num;
                }).slice(0, setting.quantity);

            var dataSizeSet = _.sortBy(
                _.uniq(
                    _.map(data, function(d) {
                        return d[2];
                    })),
                function(num) {
                    return num;
                });

            var width = setting.size,
                height = setting.size,
                margin = setting.margin;


            var centralRadius = setting.size / 15,
                corRadius = centralRadius,
                otherRadius = centralRadius / 1.5,
                centralColor = setting.centralColor,
                nomColor = setting.marginBlockColor,
                corNum = setting.correlation;


            // Scale
            var groupScale = function(num) {
                    for (var i = dataSizeSet.length - 1; i >= 0; i--) {
                        if (num === dataSizeSet[i]) {
                            return i + 1;
                        }
                    }
                },
                sideScale = d3.scale.linear()
                .domain(d3.extent(dataSizeSet))
                .range([centralRadius * 0.3, centralRadius * 0.7]);



            var centralData = getCentralData(data),
                forceTreeDataNodes = setForceTreeDataNodes(data),
                forceTreeDataLinks = setForceTreeDataLinks(forceTreeDataNodes);

            var distanceScale = d3.scale.linear()
                .domain(d3.extent(dataSizeSet))
                .range([4.5 * centralRadius, 2.5 * centralRadius]);
            // Copy and Paste

            var color = centralColor;

            var force = d3.layout.force()
                .charge(setting.charge)
                .linkDistance(function(d) {
                    return distanceScale(d.value);
                })
                .size([width, height])
                .gravity(setting.gravity)
                .friction(setting.friction);
            // .theta(setting.theta);

            var svg = container.append("svg")
                .attr("width", width)
                .attr("height", height)
                .attr("class", "corrgraph");


            force
                .nodes(forceTreeDataNodes)
                .links(forceTreeDataLinks)
                .start();

            var lineAttrs = {
                "class": function(d, i) {
                    return 'link ' + "line-" + (i + 1);
                },
                "stroke-width": 4,
                'stroke': 'lightgray'
            };
            var links = svg.selectAll(".link")
                .data(forceTreeDataLinks)
                .enter()
                .append("line")
                .attr(lineAttrs)
                .style("cursor", "pointer");

            var nodes = svg.selectAll(".nodes")
                .data(forceTreeDataNodes)
                .enter().append("g").attr('class', 'nodes')
                .attr('class', function(d, i) {
                    if (i === 0) {
                        return 'centralNode';
                    }
                });
            var colors20 = d3.scale.category20c();
            var colors = [];
            for( var i = 0; i < 20; i++) {
                colors.push(colors20(i));
            }
            var colorScale = d3.scale.linear()
                               .domain(d3.range(20))
                               .range(colors);
            var circles = nodes.append("circle").style("fill", function(d, i) {
                // COLOR TRANSIT
                if (i === 0) {
                    return centralColor;
                } else if (i <= corNum) {
                    if (typeof(color) === 'function') {
                        return color(d.group);
                    } else if (_.isArray(color) === true) {
                        return color[(i % color.length)];
                    } else {
                        return d3.rgb(color);
                    }

                } else {
                    return nomColor;
                }

                
            }).attr("class", function(d, i) {
                return 'circle-' + i;
            });

            var texts = nodes.append("text")
                .attr("width", function(d) {
                    return d.group;
                })
                .text(function(d) {
                    return d.name;
                });
            
            force.on("tick", function() {

                var circleAttrs = {
                    'cx': function(d, i) {
                        return d.x;
                    },
                    'cy': function(d, i) {
                        return d.y;
                    },
                    'r': '3'
                };
                circles.attr(circleAttrs);

                var textAttrs = {
                    "text-anchor": "middle",
                    "font-size": function(d, i) {

                        return centralRadius / 3;
                       
                    },
                    "font-weight": function(d, i) {
                        if (i <= corNum) {
                            return 700;
                        }
                    },
                    "x": function(d, i) {
                        return d.x;
                        // if (i === 0) {
                        //     return d.x + textWidth / 2 + 5;
                        // } else {

                        //     return d.x + textWidth / 2 + 5;
                        // }

                    },
                    "y": function(d, i) {
                        return d.y;
                        // if (i === 0) {
                        //     return d.y + centralRadius / 2;
                        // } else if (i <= corNum) {
                        //     return d.y + centralRadius / 6 + 5;
                        // } else {
                        //     return d.y + 10;
                        // }
                    }
                };

                texts.attr(textAttrs);

                circleSizeAttrs = {
                    // "width": function(d, i) {
                    //     return textsWidth[i] + 10;
                    // },
                    "r": function(d, i) {
                        if (i <= corNum) {
                            return corRadius+3;
                        } else {
                            return otherRadius+3;
                        }
                    }
                };

                var linkAttrs = {
                    'x1': function(d, i) {
                        return d.source.x;
                    },
                    'y1': function(d, i) {
                        return d.source.y;
                    },
                    'x2': function(d, i) {
                        return d.target.x;
                    },
                    'y2': function(d, i) {
                        return d.target.y;
                    }
                };
                links.attr(linkAttrs);
                circles.attr(circleSizeAttrs);


                    


            });


            // Adjust texts' width
            var textsWidth = [];
            texts.text(function(d,i) {
                textsWidth[i] = this.getComputedTextLength();
                var perWordLength = textsWidth[i] / d.name.length;;
                if (i <= corNum) {
                    if (textsWidth[i] >= centralRadius*2 ) {

                        var maxNum = ~~ (centralRadius*2 / perWordLength) - 4;
                        var result = d.name.slice(0, maxNum) + "...";
                        return result;
                    } else {
                        return d.name;
                    }
                } else {
                    if (textsWidth[i] >= otherRadius*2 ) {
                        var maxNum = ~~ (otherRadius*2 / perWordLength) - 6;
                        var result = d.name.slice(0, maxNum) + "...";
                        return result;
                    } else {
                        return d.name;
                    }
                }
            }).attr("fullText", function(d, i){
                if(i<= corNum && textsWidth[i]>= centralRadius * 2) {
                    return d.name
                } else if(i> corNum && textsWidth[i] >= otherRadius * 2) {
                    return d.name;    
                } else {
                    return;
                }
            });

            // View adjust
            var circleStyle = {
                'z-index': 2,
                'cursor': 'pointer',
                'opacity': 1,
                'stroke-width': 3,
            };
            circles.style(circleStyle);


            var textsStyle = {
                'cursor': 'pointer'
            };

            texts.style(textsStyle);



            // event
            if (setting.draggable === true) {
                nodes.call(force.drag);
            }

            links.on('mouseover', function() {
                var link = d3.select(this);
                link.transition().duration(500)
                    .style('stroke', 'black')
                    .style('stroke-width', '6');
            });
            nodes.on('mouseover', function() {
                var circle = d3.select(this).select('circle'),
                    text = d3.select(this).selectAll('text'),
                    textWidth = text.node().getComputedTextLength(),
                    radius = circle.attr("r");
                if(text.attr("fullText")) {
                    text.transition().duration(500)
                        .style('opacity', 1);

                    var pendingText= text.attr("fullText");
                    text.attr("fullText", text.text());
                    text.text(pendingText)

                    circle.attr("r", function(d) {
                        return radius*2.5 ;
                    });
                    circle.attr("originRadius", function(d) {
                        return radius;
                    });
                }

                circle.transition().duration(300)
                    .style('opacity', 1);
                if (d3.select(this).select('.circle-0')[0][0] === null) {
                    var idNum = circle.attr('class').replace(/circle-/, '');
                    d3.select('.line-' + idNum).transition()
                        .duration(500)
                        .style('stroke', 'black')
                        .style('stroke-width', '6');
                    d3.select('.circle-0').transition()
                        .duration(500).style('opacity', 1);

                }
            });

            nodes.on('mouseout', function() {
                var circle = d3.select(this).select('circle'),
                    radius = circle.attr("originRadius");

                var text = d3.select(this).selectAll('text'),
                    textWidth = text.node().getComputedTextLength();

                if (radius) {
                    var pendingText= text.attr("fullText");
                    text.attr("fullText", text.text());
                    text.text(pendingText);
                    if (circle.attr("originRadius")) {
                        circle.attr("r", function(d) {
                            return circle.attr("originRadius");
                        });
                    }
                }



                if (d3.select(this).select('.circle-0')[0][0] === null) {
                    var idNum = circle.attr('class').replace(/circle-/, '');
                    d3.select('.line-' + idNum).transition()
                        .duration(500)
                        .style('stroke', 'lightgray')
                        .style('stroke-width', '4');

                }

            });

            links.on('mouseout', function() {
                var link = d3.select(this);
                link.transition().duration(500)
                    .style('stroke', 'lightgray')
                    .style('stroke-width', '4');
            });
            var defaultEdgeFunc = function() {
                // The dataId has to minus one, since '0' is the centralData
                var dataId = parseInt(d3.select(this)
                    .attr("class")
                    .match(/line-(\d+)/)[1]) - 1;
                var data = derivedData[dataId];


                // LOAD DATA
                d3.select('#correlationTreeSVG').transition().remove();
                
                args.targetword = data[1];
                var edgeAction = args.EdgeFunc||drawEdge;
                loadEdgeData(args,settingInput,container,edgeAction);
            }


            links.on('click',defaultEdgeFunc);



            nodes.on("click", function() {
                // $('#corrLabel').fadeOut();
                if (d3.select(this).select('.circle-0')[0][0] === null) {
                    var centralCircle = svg.select('.circle-0')[0][0];

                    var cp = getCentralPointPosition(centralCircle);
                    // fade out
                    var circles = nodes.selectAll("circle");
                    circles.transition()
                        .duration(500)
                        .attr(cp)
                        .attr('width', 0)
                        .attr('height', 0)
                        .style('opacity', 0);
                    texts.transition()
                        .duration(500)
                        .attr(cp)
                        .style('opacity', 0);
                    links.transition()
                        .duration(300)
                        .style('opacity', 0);
                    regenerateFlag = true;

                    // load data
                    var dataName = d3.select(this).select('text').text();
                    // d3.select('#correlationTreeSVG').transition().delay(500).remove();

                    setTimeout(function() {
                        recordHistory(args);
                        container.html("");
                        args.keyword = dataName;
                        loadData(args,settingInput,container,drawCorr);
                    }, 700);
                }
            });

            function getCentralPointPosition(centralCircle) {
                var cp = {
                    'x': d3.select(centralCircle).attr('cx'),
                    'y': d3.select(centralCircle).attr('cy')
                };
                return cp;
            }


            function getCentralData(dataSet) {
                if (dataSet[0] !== undefined) {
                    return dataSet[0][0];
                }
            }

            function setForceTreeDataNodes(dataSet) {
                var result = [{
                    'x': width / 2,
                    'y': height / 2,
                    'name': centralData,
                    'group': 0,
                    'fixed': true
                }];
                _.each(dataSet, function(d, i) {
                    var obj = {
                        'name': d[1],
                        'group': groupScale(d[2])
                    };
                    if (groupScale(d[2]) <= dataSizeSet.length - corNum) {
                        obj.group = 1;
                    }
                    result.push(obj);
                });
                return result;
            }

            function setForceTreeDataLinks(dataNodeSet) {
                var result = [];
                _.each(dataNodeSet, function(d, i) {
                    if (i !== 0) {
                        var obj;

                        obj = {
                            "source": 0,
                            "target": i,
                            "value": dataSizeSet[d.group - 1]
                        };
                        result.push(obj);

                    }
                });
                return result;
            }
        }

        function recordHistory(args) {
            var record = {};
            record.keyword = args.keyword;
            record.targetword = args.targetword
            record.assoc = args.assoc;
            history.push(record);

        }

        function initBasicSetting(setting) {
            setting.size = setting.size ? setting.size : 480;
            setting.textColor = setting.textColor ? setting.textColor : 'black';
            // setting.color = setting.color ? setting.color : d3.scale.category20();
            setting.margin = setting.margin ? setting.margin : 10;
            return setting;
        }

        function initCorrSetting(setting, data) {
            setting = initBasicSetting(setting);
            setting.quantity = setting.quantity ? setting.quantity : 20;
            setting.centralColor = setting.centralColor ? setting.centralColor : 'tomato';
            setting.marginBlockColor = setting.marginBlockColor ? setting.marginBlockColor : '#ddd';
            setting.correlation = (setting.correlation && setting.correlation >= 1) ? setting.correlation : 5;
            // if (setting.draggable !== true) {
            //     setting.draggable = false;
            // }
            setting.gravity = (setting.gravity >= 0 && setting.gravity <= 1) ? setting.gravity : 0.2;
            setting.charge = setting.charge ? setting.charge : -120;
            setting.friction = setting.friction ? setting.friction : 0.9;
            setting.theta = setting.theta ? setting.theta : 0.8;
            return setting;
        }
        return this;
    };

})(window); 
