# author: oskar.blom@gmail.com
#
# Make sure your gevent version is >= 1.0
import gevent
from gevent.wsgi import WSGIServer
from gevent.queue import Queue
from flask import Flask, Response,request
import flask
import requests
import json

# SSE "protocol" is described here: http://mzl.la/UPFyxY
class ServerSentEvent(object):

    def __init__(self, data):
        self.data = data
        self.event = None
        self.id = None
        self.desc_map = {
            self.data : "data",
            self.event : "event",
            self.id : "id"
        }

    def encode(self):
        if not self.data:
            return ""
        lines = ["%s: %s" % (v, k) 
                 for k, v in self.desc_map.iteritems() if k]
        
        return "%s\n\n" % "\n".join(lines)

app = Flask(__name__,static_url_path='')
subscriptions = []

@app.route('/static/<path:path>')
def send_js(path):
    return flask.send_from_directory('static', path)

# Client code consumes like this.
@app.route("/")
def index():
    return flask.render_template('demo.html')

@app.route("/debug")
def debug():
    return "Currently %d subscriptions" % len(subscriptions)



def cmd2JSON(cmd):
   return json.dumps({'Stmt':cmd,'Workspace':"",'Opts':{}})

def printdata(data_str):
        data = json.loads(data_str)
        count = 0
        if(type(data['Content']) != dict):
                if json.dumps(data['Content']) != "null":
                        if data['Content'] != "":
                                print(json.dumps(data['Content'], indent=4))
                else:   
                        if data['Err']!= "":
                                print(json.dumps(data['Err'], indent=4))
                return 0

        if 'content' in data['Content'].keys():
                for row in data['Content']['content']:
                        #print(row)
                        print_row=""
                        for record in row:
                                if print_row != "":
                                        print_row += ","
                                print_row += str(record).decode('utf-8')
                        print(print_row)
                        #print(row)
                        count+=1
        else:   
                if data['Content'] != "":
                        print(json.dumps(data['Content'], indent=4))

        return count

def getData(cmdStr):
        r = requests.post('http://bigobject:9090/cmd',data=cmd2JSON(cmdStr) , stream=True )
        data=[]
        for jsonobj in json_stream(r.raw):
              data.extend(jsonobj['Content']['content'])
        return data

def json_stream(fp):
        for line in fp:
                #print(line)
                yield json.loads(line)


#def getData(cmdStr):
#    r = requests.post('http://localhost:9090/cmd',data=cmd2JSON(cmdStr))
#    return r.json()['Content']['content']


   
cmdDict={
      'exception':{'server':"find server,result='Error' , count(result) from chalet529 ",
                   'server+sqlcmd':"find sqlcmd, server, result='Error' ,count(result) from chalet529",
                   'user':"find user, result='Error',count(result) from chalet529",
                  },
      'compare' :{'time_exception_user': "find user, result='Error',count(result) from chalet529 {filter: NOT IN 528fail}"
                 },
      'usage'   :{'top5server' : "find top 5 server, sum(total_time) from chalet529 ",
                  'top5user' : "find top 5 user ,sum(total_time) from chalet529",
                  'top5db'   : "find top 5 db, sum(total_time) from chalet529"
                 }, 
      'responsetime':{'top5server': "find top 5 server, avg(resp_time) from chalet529 {filter: IN server_load}",
                      'topserver+sqlcmd' : "find sqlcmd, server='192.168.136.128:Oracle', avg(resp_time) from chalet529"
                 },
      'significance':{'cluster_server+sqlcmd':"find sqlcmd, server, count(server) from chalet529, cluster3",
                      'cluster_server+client':"find client, server, count(server) from chalet529, cluster3"
                 }

}

def getCmd(cmd,subject):
    return cmdDict[cmd][subject]
 
    

@app.route("/event",methods=['POST'])
def publish():
    #Dummy data - pick up from request for real data
    request.get_data()
    tmplist = request.data.split(',')
    exception=tmplist[0]
    user,server=tmplist[1].split('@')
    print user, server ,exception
     
    userErr="find dbuser,time,count(dbuser) from chalet_event2 {{filter:dbuser='{0}' and time > '2015-08-7' and result='Error'}}".format(user) 
    serverErr="find dbserver,time,count(dbserver) from chalet_event2 {{filter:dbserver='{0}' and time > '2015-08-7' and result='Error'}}".format(server) 
    packet="find dbuser,time,sum(pnum) from packet {filter: dbuser in ('Ethan','JASON','Frank','Mary','Jocelyn','Mary','Amy','Carol','Daniel')}"
#    packet="find 101 dbuser,time,sum(pnum) from packet {filter: dbuser in ('jack','JASON','Frank','Mary','Mark')}"

    jsondata={'title':exception,'user':user}
    userdata = getData(userErr)
    serverdata = getData(serverErr)
    
    if exception=='Large_Record' and user=='JASON':
       packetdata = getData(packet)
       jsondata['pdata']=packetdata
    
    jsondata['udata']=userdata
    jsondata['sdata']=serverdata
    def notify(data):
        msg = data
        for sub in subscriptions[:]:
            sub.put(msg)
    
    gevent.spawn(notify,jsondata)
    
    return "OK"

@app.route("/stream")
def subscribe():
    def gen():
        q = Queue()
        subscriptions.append(q)
        try:
            while True:
                result = q.get()
                ev = ServerSentEvent(json.dumps(result))
                #ev = ServerSentEvent(str(result))
                yield ev.encode()
        except GeneratorExit: # Or maybe use flask signals
            subscriptions.remove(q)

    return Response(gen(), mimetype="text/event-stream")

def main():
    app.debug = True
    server = WSGIServer(("", 7777), app)
    server.serve_forever()
 
if __name__ == "__main__":
    main()

