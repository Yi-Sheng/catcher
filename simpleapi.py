import requests
import json
import sys

def cmd2JSON(cmd):
    return json.dumps({'Stmt':cmd,'Workspace':"",'Opts':""})

def getData(server,cmdStr):
    data=[]
    r = requests.post(server,data=cmd2JSON(cmdStr))
    key = json.loads( r.json()[0]['Content'])[0]
    print key
    while True:
         rdict = json.loads(requests.post(server,data=cmd2JSON('scan '+ key)).json()[0]['Content'])
         data.extend(rdict['content'])
         if rdict['index']==-1: break
    return data 


if __name__ == "__main__":
    #server = sys.argv[1]
    #cmd = sys.argv[2]
    server = "http://127.0.0.1:9090/cmd"
    cmd    = "select * from sales"
    print getData(server,cmd)    
